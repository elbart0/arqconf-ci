FROM centos:latest
MAINTAINER mruggieri
RUN yum update -y && yum -y install httpd
ADD artifact.tag.gz     /var/www/html/
CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]
EXPOSE 80

